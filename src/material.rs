//! Parameters for material modeling.
//!
//! ### Citations
//!
//! Permittivities (under 1kHz unless otherwise specified) sourced from
//! [Wikipedia](https://en.wikipedia.org/wiki/Relative_permittivity), excepting
//! the value for Rexolite, which is from _Electromagnetic Fields and Waves_.

/// Permittivity of free space.
pub static EPS_FREE_SPACE: &'static f64 = &8.854187817620e-12;
/// Normalized permittivity of free space (for relative permittivities).
pub static EPS_FREE_SPACE_NORMAL: &'static f64 = &1.0;
/// Relative permittivity of air (STP, 0.9MHz).
pub static EPS_AIR: &'static f64 = &1.00058986;
/// Relative permittivity of Teflon (PTFE).
pub static EPS_PTFE: &'static f64 = &2.1;
/// Relative permittivity of polyethylene (XLPE)
pub static EPS_XLPE: &'static f64 = &2.25;
/// Relative permittivity of polypropylene (ranges from 2.2-2.36).
pub static EPS_POLYPROPYLENE: &'static f64 = &2.28;
/// Relative permittivity of polystyrene (ranges from 2.4-2.7).
pub static EPS_POLYSTYRENE: &'static f64 = &2.55;
/// Relative permittivity of carbon disulfide.
pub static EPS_CARBON_DISULFIDE: &'static f64 = &2.6;
/// Relative permittivity of Rexolite.
pub static EPS_REXOLITE: &'static f64 = &2.65;
/// Relative permittivity of Mylar.
pub static EPS_MYLAR: &'static f64 = &3.1;
/// Relative permittivity of polyimide.
pub static EPS_POLYIMIDE: &'static f64 = &3.4;
/// Relative permittivity of paper.
pub static EPS_PAPER: &'static f64 = &3.85;
/// Relative permittivity of silicon dioxide.
pub static EPS_SILICON_DIOXIDE: &'static f64 = &3.9;
/// Relative permittivity of mica (ranges from 3-6).
pub static EPS_MICA: &'static f64 = &4.5;
/// Relative permittivity of concrete.
pub static EPS_CONCRETE: &'static f64 = &4.5;
/// Relative permittivity of Pyrex (ranges from 3.7-10).
pub static EPS_PYREX: &'static f64 = &4.7;
/// Relative permittivity of neoprene.
pub static EPS_NEOPRENE: &'static f64 = &6.7;
/// Relative permittivity of rubber.
pub static EPS_RUBBER: &'static f64 = &7.0;
/// Relative permittivity of silicon nitride (Si3N4) (ranges from 7-8 for
/// polycrystalline samples at 1MHz).
pub static EPS_SILICON_NITRIDE: &'static f64 = &7.5;
/// Relative permittivity of diamond (ranges from 5.5-10).
pub static EPS_DIAMOND: &'static f64 = &7.75;
/// Relative permittivity of salt (ranges from 3-15).
pub static EPS_SALT: &'static f64 = &9.0;
/// Relative permittivity of hydrocyanic acid (HCN) (at 20 degC).
pub static EPS_HYDROCYANIC_ACID: &'static f64 = &9.71;
/// Relative permittivity of sapphire (ranges from 8.9-11.1).
pub static EPS_SAPPHIRE: &'static f64 = &10.0;
/// Relative permittivity of silicon.
pub static EPS_SILICON: &'static f64 = &11.68;
/// Relative permittivity of graphite (ranges from 10-15).
pub static EPS_GRAPHITE: &'static f64 = &12.5;
/// Relative permittivity of ammonia (NH4+) (at 20 degC).
pub static EPS_AMMONIA: &'static f64 = &17.0;
/// Relative permittivity of methanol (CH3OH).
pub static EPS_METHANOL: &'static f64 = &30.0;
/// Relative permittivity of ethylene glycol ((CH2OH)2).
pub static EPS_ETHYLENE_GLYCOL: &'static f64 = &37.0;
/// Relative permittivity of furfural.
pub static EPS_FURFURAL: &'static f64 = &42.0;
/// Relative permittivity of glycerol (at 20 degC).
pub static EPS_GLYCEROL: &'static f64 = &47.0;
/// Relative permittivity of water (at 20 degC).
pub static EPS_WATER: &'static f64 = &80.1;
/// Relative permittivity of hydrofluoric acid (at 0 degC).
pub static EPS_HF: &'static f64 = &83.6;
/// Relative permittivity of hydrazine (at 20 degC).
pub static EPS_HYDRAZINE: &'static f64 = &52.0;
/// Relative permittivity of formamide (at 20 degC).
pub static EPS_FORMAMIDE: &'static f64 = &84.0;
/// Relative permittivity of sulfuric acid (H2SO4) (at 20 degC).
pub static EPS_SULFURIC_ACID: &'static f64 = &84.0;
/// Relative permittivity of hydrogen peroxide (H2O2) (at 20 degC).
pub static EPS_HYDROGEN_PEROXIDE: &'static f64 = &66.2;
/// Relative permittivity of titanium dioxide (TiO2) (ranges from 86-173).
pub static EPS_TITANIUM_DIOXIDE: &'static f64 = &129.5;
/// Relative permittivity of strontium titanate (SrTiO3).
pub static EPS_STRONTIUM_TITANATE: &'static f64 = &310.0;
/// Relative permittivity of barium strontium titanate (BaSrTiO4).
pub static EPS_BARIUM_STRONTIUM_TITANATE: &'static f64 = &500.0;
