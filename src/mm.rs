//! Functions for method-of-moments calculations. Includes implementations for
//! multiple topologies, since anisomorphic topologies require another
//! implementation (cf. generalized method of moments).

use std::any::Any;

use num::{Float, NumCast};

use rulinalg::vector::Vector;
use rulinalg::error::Error;
use rulinalg::matrix::{BaseMatrix, BaseMatrixMut, Matrix, DiagOffset};

use ElectroGrid;
use topo::{Cyl, ParallelPlate, pairwise_dist, TAU};
use material::EPS_FREE_SPACE;

// Gets the pair index from a matrix. Assumes row-major (but works with either).
fn indices_from_pos(row_stride: usize, ind: usize) -> [usize; 2] {
  let col_ind = ind % row_stride;
  let row_ind = ind / row_stride;
  [row_ind, col_ind]
}

/// Method of moments methods for computing electrical charge distributions on a
/// surface.
pub trait MethodOfMoments<T,U>
  where T: NumCast + Float + Any, U: Float {
  type Output;
  /// Computes the pairwise distance between grid elements, one row per element.
  fn row_pairwise_dists(&self) -> Vec<Vec<U>>;
  /// Computes the intercept vector used in the matrix equation.
  fn intercept_vec(&self) -> Vector<T>;
  /// Computes the matrix that left divides the intercept vector.
  fn moment_matrix(&self) -> Matrix<T>;
  /// Computes the charge distribution based on the given topology.
  fn moment_comp(self) -> Result<Self::Output, Error>;  
}

/// An implementation of the method of moments for parallel plate topologies.
impl<T,U> MethodOfMoments<T,U> for ParallelPlate<T,U>
  where T: NumCast + Float + Any, U: Float {
  type Output = ParallelPlate<T,U>;
  
  fn row_pairwise_dists(&self) -> Vec<Vec<U>> {
    // TODO reduce copying here
    let mut grid_space_vec: Vec<[U; 2]> = self.top_grid.space.data().clone();
    grid_space_vec.extend(self.bottom_grid.space.data());
    
    grid_space_vec
      .iter()
      .map(|coord_arr: &[U; 2]| {
        // We need to skip the current coordinate pair itself, but compute
        // every other distance.
        grid_space_vec.iter().map(|&other_coord_arr| {
          pairwise_dist(coord_arr, other_coord_arr)
        }).collect::<Vec<U>>()
      }).collect::<Vec<Vec<U>>>()
  }

  fn intercept_vec(&self) -> Vector<T> {
    // TODO reduce copying here
    let mut grid = self.top_grid.potential.data().clone();
    grid.extend(self.bottom_grid.potential.data());
    Vector::<T>::from(grid.clone())
  }

  fn moment_matrix(&self) -> Matrix<T> {
    // Compute the distances from each point to each other point, so we don't
    // have to recompute every time.
    //
    // TODO memoize this in the struct or something
    let distances: Vec<Vec<U>> = self.row_pairwise_dists();
    let top_grid_size = self.top_grid.space.data().len();

    let two_cast_tau: U = NumCast::from(2.0 * TAU).unwrap();
    
    // Now we can finally set up the actual matrix
    let moment_vec: Vec<T> = distances
      .iter()
      .flat_map(move |dist_row| {
        dist_row.iter().enumerate().map(move |(el_ind, &dist)| {

          if dist != NumCast::from(0).unwrap() {
            // Since we were iterating over the MxN elements of the matrix, we
            // need to convert back to the usual dimensional indices for sanity.

            // Check to see if we're in the area of values matching the top
            // plate grid
            if el_ind < top_grid_size {
              let current_pos = indices_from_pos(self.top_grid.space.row_stride(), el_ind);
              
              NumCast::from(self.top_grid.space_area(current_pos) /
                            two_cast_tau / dist)
                .unwrap()
            } else {
              let current_pos = indices_from_pos(self.bottom_grid.space.row_stride(),
                                                 el_ind - top_grid_size);
              
              NumCast::from(self.bottom_grid.space_area(current_pos) /
                            two_cast_tau / dist)
                .unwrap()
            }            

          } else {
            // This should only occur in the diagonal case. It's only a
            // placeholder until the diagonal correction down below.
            NumCast::from(0).unwrap()
          }        
        })
      }).collect();
    
    let moment_dim = self.top_grid.space.rows() * self.top_grid.space.cols() +
      self.bottom_grid.space.rows() * self.bottom_grid.space.cols();
    
    let mut moment_mat: Matrix<T> = Matrix::new(moment_dim,
                                                moment_dim,
                                                moment_vec);
    
    // Correct the diagonal values (rectangular case)
    let two_tau_recip: T = NumCast::from(1.0 / (2.0 * TAU).sqrt()).unwrap();
    let top_grid_size = self.top_grid.space.data().len();

    for (diag_ind, mut diag_val) in moment_mat
      .diag_iter_mut(DiagOffset::Main)
      .enumerate() {
        // Check to see if we're in the area of values matching the top plate
        // grid
        if diag_ind < top_grid_size {
          let current_pos = indices_from_pos(self.top_grid.space.row_stride(),
                                             diag_ind);
          *diag_val = two_tau_recip *
            NumCast::from(self.top_grid.space_area(current_pos).sqrt()).unwrap() /
            self.top_grid.permittivity[current_pos];
        } else {
          let offset_ind = diag_ind - top_grid_size;
          let current_pos = indices_from_pos(self.bottom_grid.space.row_stride(),
                                             offset_ind);
          
          *diag_val = two_tau_recip *
            NumCast::from(self.bottom_grid.space_area(current_pos).sqrt()).unwrap() /
            self.top_grid.permittivity[current_pos];
        }
      }

    moment_mat
  }

  /// Computes the charge distribution over a pair of rectangular surfaces
  /// represented by a field matrix.
  fn moment_comp(self) -> Result<Self::Output, Error> {
    // First compute the vector to invert with
    let intercept_vec: Vector<T> = self.intercept_vec(); 

    let moment_mat = self.moment_matrix();

    moment_mat.solve(intercept_vec).and_then(move |charge_vector| {
      let mut charge_vec = charge_vector.into_vec();
      
      let top_grid_size = self.top_grid.space.data().len();
      let bottom_charge = Matrix::new(self.bottom_grid.space.rows(),
                                      self.bottom_grid.space.cols(),
                                      // Split off the "bottom half" of the
                                      // charge
                                      charge_vec.split_off(top_grid_size));

      let top_charge = Matrix::new(self.top_grid.space.rows(),
                                   self.top_grid.space.cols(),
                                   charge_vec);

      let new_top_grid: ElectroGrid<T,U> = ElectroGrid {
        charge: Some(top_charge), ..self.top_grid
      };

      let new_bottom_grid: ElectroGrid<T,U> = ElectroGrid {
        charge: Some(bottom_charge), ..self.bottom_grid
      };
        
      Ok(ParallelPlate {
        bottom_grid: new_bottom_grid,
        top_grid: new_top_grid,
        plate_spacing: self.plate_spacing
      })
    })
  }
}

/// An implementation of the method of moments for cylindrical topologies.
impl<T,U> MethodOfMoments<T,U> for Cyl<T,U>
  where T: NumCast + Float + Any, U: Float {
  type Output = Cyl<T,U>;

  fn row_pairwise_dists(&self) -> Vec<Vec<U>> {
    self.grid
        .space
        .data()
        .iter()
        .map(|coord_arr: &[U; 2]| {
          self.grid.space.data().iter().map(|&other_coord_arr| {
            pairwise_dist(coord_arr, other_coord_arr)
          }).collect::<Vec<U>>()
        }).collect::<Vec<Vec<U>>>()
  }

  fn intercept_vec(&self) -> Vector<T> {
    let two_tau: f64 = 2.0 * TAU;
    let two_tau_eps: T = NumCast::from(two_tau * EPS_FREE_SPACE).unwrap();
    
    Vector::<T>::from(
      self.grid
          .potential
          .data()
          .iter()
          .zip(self.grid.permittivity.data().iter())
          .map(|(&potential_el, &permittivity_el)| {
            two_tau_eps * 
              NumCast::from(self.radius).unwrap() * 
              NumCast::from(self.slice_width).unwrap() * 
              potential_el * 
              permittivity_el
          }).collect::<Vec<T>>())
  }

  fn moment_matrix(&self) -> Matrix<T> {
    // Compute the distances from each point to each other point, so we don't
    // have to recompute every time.
    //
    // TODO memoize this in the struct or something
    let distances: Vec<Vec<U>> = self.row_pairwise_dists();

    let cast_tau: T = NumCast::from(TAU).unwrap();
    
    // Now we can finally set up the actual matrix
    let slice_width = self.slice_width.clone();
    let radius = self.radius.clone();
    let spatial_zero: U = NumCast::from(0).unwrap();
    let mixed_zero: T = NumCast::from(0).unwrap();
    let moment_vec: Vec<T> = distances.iter().flat_map(move |dist_row| {
      dist_row.iter().map(move |&dist| {
        if dist != spatial_zero {
          cast_tau * NumCast::from(slice_width * radius / dist).unwrap()
        } else {
          mixed_zero
        }
      })
    }).collect();
    
    let moment_dim = self.grid.space.rows() * self.grid.space.cols();
    
    let mut moment_mat: Matrix<T> = Matrix::new(moment_dim,
                                                moment_dim,
                                                moment_vec);
    
    // Correct the diagonal values (cylindrical case)
    let two_tau_cast: T = NumCast::from(TAU * 2.0).unwrap();
    for mut diag_val in moment_mat.diag_iter_mut(DiagOffset::Main) {
      *diag_val = two_tau_cast * NumCast::from((self.slice_width / self.radius).ln()).unwrap();
    }

    moment_mat
  }

  /// Computes the charge distribution over a cylindrical surface represented by
  /// a field matrix.
  fn moment_comp(self) -> Result<Self::Output, Error> {
    // First compute the vector to invert with
    let intercept_vec: Vector<T> = self.intercept_vec(); 

    let moment_mat = self.moment_matrix();

    moment_mat.solve(intercept_vec).and_then(move |charge_vec| {
      let out_charge = Matrix::new(self.grid.space.rows(),
                                   self.grid.space.cols(),
                                   charge_vec);
      let new_grid: ElectroGrid<T,U> = ElectroGrid {
        charge: Some(out_charge), ..self.grid
      };
      
      Ok(Cyl {
        grid: new_grid,
        radius: self.radius,
        slice_width: self.slice_width
      })
    })
  }
}
