//! Functions for method-of-moments calculations.
use std::any::Any;
use std::iter::Sum;
use std::cmp::Eq;

use num::{Float, NumCast};

// use rulinalg::vector::Vector;
use rulinalg::error::{Error, ErrorKind};
use rulinalg::matrix::{BaseMatrix, Matrix};

use ElectroGrid;
// use material::EPS_FREE_SPACE;

/// A triangular element.
#[derive(Debug)]
pub struct TriEl<T, U> where T: NumCast + Float + Any, U: Float {
  /// The (rectangular) coordinates of each point, in counterclockwise order.
  pub space: [[U; 2]; 3],
  /// The electric permittivity within the element.
  pub permittivity: T,
  /// The grid nodes the element's vertices correspond to (in CCW order)
  pub nodes: [usize; 3]
}

/// Functions for the triangular element.
impl<T,U> TriEl<T,U> where T: NumCast + Float + Any, U: Float {
  /// Computes half the determinant, or the area, of the triangle expressed by
  /// the set of coordinates.
  fn area(&self) -> U {
    let coords = self.space;

    (coords[0][0] * coords[1][1] - coords[2][0] * coords[0][1] +
     coords[2][0] * coords[0][1] - coords[0][0] * coords[2][1] +
     coords[1][0] * coords[2][1] - coords[2][0] * coords[1][1])
      / NumCast::from(2.0).unwrap()
  }

  fn stiffness_mat(&self) -> Matrix<T> {
    let quarter: T = NumCast::from(1.0 / 4.0).unwrap();
    let area_coef: T = quarter / NumCast::from(self.area()).unwrap();

    let coords = self.space;

    let x1x3: T = NumCast::from(coords[0][0] - coords[2][0]).unwrap();
    let x2x1: T = NumCast::from(coords[1][0] - coords[0][0]).unwrap();
    let x3x2: T = NumCast::from(coords[2][0] - coords[1][0]).unwrap();
    let y1y2: T = NumCast::from(coords[0][1] - coords[1][1]).unwrap();
    let y2y3: T = NumCast::from(coords[2][1] - coords[2][1]).unwrap();
    let y3y1: T = NumCast::from(coords[2][1] - coords[0][1]).unwrap();

    // TODO replace with upper diagonal matrix constructor
    let c_00: T = (y2y3.powi(2) + x3x2.powi(2)) / area_coef * self.permittivity;
    let c_01: T = (y2y3 * y3y1 + x3x2 * x1x3) / area_coef * self.permittivity;
    let c_02: T = (y2y3 * y1y2 + x3x2 * x2x1) / area_coef * self.permittivity;
    let c_10: T = c_01.clone();
    let c_11: T = (y3y1.powi(2) + x1x3.powi(2)) / area_coef * self.permittivity;
    let c_12: T = (y3y1 * y1y2 + x1x3 * x2x1) / area_coef * self.permittivity;
    let c_20: T = c_02.clone();
    let c_21: T = c_12.clone();
    let c_22: T = (y1y2.powi(2) + x2x1.powi(2)) / area_coef * self.permittivity;

    Matrix::<T>::new(3, 3, vec![c_00, c_01, c_02,
                                c_10, c_11, c_12,
                                c_20, c_21, c_22])
  }
}

/// Finite element method functions. Limited to triangular elements at the
/// moment.
pub trait FiniteElementMethod<T,U>
  where T: NumCast + Float + Any, U: Float {
  /// Computes the triangular elements from the points provided. Elements are
  /// roughly ordered from left to right by adjacent pairs. Thus, in two rows of
  /// points on a grid, there will be twice as many elements as there are points
  /// in a row.
  fn generate_elements(&self) -> Vec<TriEl<T,U>>;

  /// Assembles the global coefficient matrix from the element equations and
  /// coefficient vectors.
  fn global_coef_matrix(&self) -> Matrix<T>;

  /// Iteratively calculates the potentials until convergence. Assumes potentials
  /// at 0 are floating (but perhaps should not?)
  fn calc_potentials(&self) -> Vec<T>;
}

/// Implementation of the triangle-based FEM for objects representable in a
/// grid.
impl<T,U> FiniteElementMethod<T,U> for ElectroGrid<T,U>
  where T: NumCast + Float + Any + Sum, U: Float {
  fn generate_elements(&self) -> Vec<TriEl<T,U>> {
    let expected_capacity: usize = self.space.rows() * 2;
    let mut tri_vec: Vec<TriEl<T,U>> = Vec::with_capacity(expected_capacity);

    // TODO these bounds checks are pretty harsh
    //
    // TODO convert to flat_map and remove mutable from above collector vec
    for row_ind in 0..self.space.rows() - 1 {
      for col_ind in 0..self.space.cols() - 1 {
        let top_inds: [[usize; 2]; 3] = [[row_ind, col_ind],
                                         [row_ind + 1, col_ind + 1],
                                         [row_ind, col_ind + 1]];
        let bot_inds: [[usize; 2]; 3] = [[row_ind, col_ind],
                                         [row_ind + 1, col_ind],
                                         [row_ind + 1, col_ind + 1]];
        let top_nodes: [usize; 3] = [
          coord_to_ind(&top_inds[0], &self.space.row_stride()),
          coord_to_ind(&top_inds[1], &self.space.row_stride()),
          coord_to_ind(&top_inds[2], &self.space.row_stride())
        ];

        let bot_nodes: [usize; 3] = [
          coord_to_ind(&bot_inds[0], &self.space.row_stride()),
          coord_to_ind(&bot_inds[1], &self.space.row_stride()),
          coord_to_ind(&bot_inds[2], &self.space.row_stride())
        ];

        // TODO consider unrolling this mut+iter into single definition
        // statement
        let mut top_coords: [[U; 2]; 3] = [[NumCast::from(0).unwrap(),
                                            NumCast::from(0).unwrap()]; 3];
        let mut bot_coords: [[U; 2]; 3] = [[NumCast::from(0).unwrap(),
                                            NumCast::from(0).unwrap()]; 3];

        for (ind, &val) in top_inds.iter().enumerate() {
          top_coords[ind] = self.space[val];
          bot_coords[ind] = self.space[bot_inds[ind]];
        }

        // Since FEM assumes the permittivity is uniform throughout the
        // element, let's average the permittivities at each of the points.
        let top_tri = TriEl {
          space: top_coords,
          permittivity: top_inds
            .iter()
            .map(|&ind| self.permittivity[ind]).sum::<T>()
            / NumCast::from(3.0).unwrap(),
          nodes: top_nodes
        };
        let bot_tri = TriEl {
          space: bot_coords,
          permittivity: bot_inds
            .iter()
            .map(|&ind| self.permittivity[ind]).sum::<T>()
            / NumCast::from(3.0).unwrap(),
          nodes: bot_nodes
        };

        tri_vec.push(top_tri);
        tri_vec.push(bot_tri);
      }
    }
    tri_vec
  }

  fn global_coef_matrix(&self) -> Matrix<T> {
    // Map each element by its vertices, so that it can be located as we iterate
    // over node indices. There must be a more efficient way to do this, but
    // this works for now.

    let typed_zero: T = NumCast::from(0).unwrap();
    
    let tris: Vec<TriEl<T,U>> = self.generate_elements();

    // println!("Triangle nodes, by triangle: ");

    // println!("Coupling indices:");
    // println!("{:?}", upper_diag_inds(self.space.data().len(),
    //                                  self.space.data().len()));

    // TODO memoize calls
    let mut global_coef_mat = Matrix::<T>::zeros(self.space.data().len(),
                                                 self.space.data().len());
    for coupling_ind in upper_diag_inds(self.space.data().len(),
                                        self.space.data().len()) {
      // Since the matrix is symmetric and we assume we are working with a
      // grid of points (even if non-uniform) divided into triangles, if we
      // number the points in rows, we only have to account for four
      // directions among the seven available from each point: the central
      // point, the rightward direction, the right downward direction, and the
      // downward direction.
      //
      // We're also assuming by implementation that the local triangular
      // vertices connected to a given node are, clockwise from upper left:
      // 1, 1, 0, 0, 2, 2.
      //
      // Lastly, we assume that the following pattern is true among adjacent
      // elements:
      //
      // | Element (index ordinal) | Vertex | Direction |
      // |-------------------------|--------|-----------|
      // | 0 (min)                 | 1      | UL        |
      // | 1                       | 2      | LU        |
      // | 2                       | 1      | UR        |
      // | 3                       | 2      | DL        |
      // | 4                       | 0      | RL        |
      // | 5                       | 0      | DR        |

      // TODO this calculation is redone a million times, it should be
      // memoized up with `let tris`, and stored with a node index.
      let adjacent_stiffnesses: Vec<Matrix<T>> = tris
        .iter().filter(|&tri_mat| {
          tri_mat.nodes.contains(&coupling_ind[0])
        }).map(|tri| {
          tri.stiffness_mat()
        }).collect();

      let space_ind = ind_to_coord(&coupling_ind[0], &self.space.row_stride());

      // println!("Current position: {:?}", &space_ind);
      // println!("Node index: {}", &coupling_ind[0]);
      // println!("Number of adjacent elements: {}", &adjacent_stiffnesses.len());

      // Bounds checking:
      //
      // Bounds checking only matters on the edges, because some directions
      // will be cut out.

      // the central point
      if coupling_ind[0] == coupling_ind[1] {
        // println!("Central point");
        
        // First row
        if space_ind[0] == 0 {

          // Upper left corner
          if space_ind[1] == 0 {
            // Only adjacent are DR, RL
            // println!("Upper left corner");
            let rl_tri: T = adjacent_stiffnesses[0][[0,0]];
            let dr_tri: T = adjacent_stiffnesses[1][[0,0]];
            global_coef_mat[coupling_ind] = rl_tri + dr_tri;

            // Upper right corner
          } else if space_ind[1] == self.space.cols() - 1 {
            // Only adjacent is DL
            // println!("Upper right corner");

            let dl_tri: T = adjacent_stiffnesses[0][[2,2]];
            global_coef_mat[coupling_ind] = dl_tri;
          } else {
            // Only adjacent are DL, RL, DR
            let dl_tri: T = adjacent_stiffnesses[0][[2,2]];
            let rl_tri: T = adjacent_stiffnesses[1][[0,0]];
            let dr_tri: T = adjacent_stiffnesses[2][[0,0]];
            global_coef_mat[coupling_ind] = dl_tri + rl_tri + dr_tri;
          }

          // Last row
        } else if space_ind[0] == self.space.rows() - 1 {
          // println!("Last row");
          
          // Lower left corner
          if space_ind[1] == 0 {
            // Only adjacent is UR
            let ur_tri: T = adjacent_stiffnesses[0][[1,1]];
            global_coef_mat[coupling_ind] = ur_tri;

            // Lower right corner
          } else if space_ind[1] == self.space.cols() - 1 {
            // // println!("Lower right corner");
            
            // Only adjacent are UL, LU
            let ul_tri: T = adjacent_stiffnesses[0][[1,1]];
            let lu_tri: T = adjacent_stiffnesses[1][[2,2]];
            global_coef_mat[coupling_ind] = ul_tri + lu_tri;
          } else {
            // Only adjacent are LU, UL, UR
            let ul_tri: T = adjacent_stiffnesses[0][[1,1]];
            let lu_tri: T = adjacent_stiffnesses[1][[2,2]];
            let ur_tri: T = adjacent_stiffnesses[2][[1,1]];
            global_coef_mat[coupling_ind] = ul_tri + lu_tri + ur_tri;
          }
          // Non edge cases
        } else {
          let ul_tri: T = adjacent_stiffnesses[0][[1,1]];
          let lu_tri: T = adjacent_stiffnesses[1][[2,2]];
          let ur_tri: T = adjacent_stiffnesses[2][[1,1]];
          let dl_tri: T = adjacent_stiffnesses[3][[2,2]];
          let rl_tri: T = adjacent_stiffnesses[4][[0,0]];
          let dr_tri: T = adjacent_stiffnesses[5][[0,0]];

          global_coef_mat[coupling_ind] =
            ul_tri + lu_tri + ur_tri + dl_tri + rl_tri + dr_tri;
        }
      // the rightward direction
      } else if coupling_ind[1] == coupling_ind[0] + 1 {
        // println!("Rightward");
        
        // First row
        if space_ind[0] == 0 {
          // NOTE Upper right corner has no rightward direction
          if space_ind[1] == self.space.cols() - 1 {
            global_coef_mat[coupling_ind] = typed_zero;
          } else {
            // Only adjacent is DR
            let dr_tri: T = adjacent_stiffnesses[1][[0,2]];
            global_coef_mat[coupling_ind] = dr_tri;
          }
          // Last row
        } else if space_ind[0] == self.space.rows() - 1 {
          // NOTE Lower right corner has no rightward direction
          if coupling_ind[1] == self.space.cols() - 1 {
            global_coef_mat[coupling_ind] = typed_zero;
          } else {
            // Only adjacent is UR
            let ur_tri: T = adjacent_stiffnesses[0][[1,2]];
            global_coef_mat[coupling_ind] = ur_tri;
          }
          // Off of the edges
        } else {
          let ur_tri: T = adjacent_stiffnesses[2][[1,2]];
          let dr_tri: T = adjacent_stiffnesses[5][[0,2]];

          global_coef_mat[coupling_ind] = ur_tri + dr_tri;
        }

      // the right downward direction
      } else if coupling_ind[1] == coupling_ind[0] + self.space.rows() + 1 {
        // println!("Right downward");
        
        // First row
        if space_ind[0] == 0 {
          // NOTE Upper right corner has no right downward direction
          if space_ind[1] == self.space.cols() - 1 {
            global_coef_mat[coupling_ind] = typed_zero;
            // Upper left corner
          } else if space_ind[1] == 0 {
            // Only adjacent are DR, RL
            let dr_tri: T = adjacent_stiffnesses[0][[0,1]];
            let rl_tri: T = adjacent_stiffnesses[1][[0,2]];
            global_coef_mat[coupling_ind] = dr_tri + rl_tri;
          } else {
            let dr_tri: T = adjacent_stiffnesses[1][[0,1]];
            let rl_tri: T = adjacent_stiffnesses[2][[0,2]];
            global_coef_mat[coupling_ind] = dr_tri + rl_tri;
          }
          // Last row
        } else if space_ind[0] == self.space.rows() - 1 {
          global_coef_mat[coupling_ind] = typed_zero;
        }
      // the downward direction
      } else if coupling_ind[1] == coupling_ind[0] + self.space.rows() {
        // println!("Downward");
        
        // First row
        if space_ind[0] == 0 {
          // Upper left corner
          if space_ind[1] == 0 {
            // println!("Upper left corner");
            
            // Only adjacent are DR, RL
            let rl_tri: T = adjacent_stiffnesses[1][[0,1]];
            global_coef_mat[coupling_ind] = rl_tri;
          } else if space_ind[1] == self.space.cols() - 1 {
            // Upper right corner
            // println!("Upper right corner");

            // Only adjacent is DL
            let dl_tri: T = adjacent_stiffnesses[0][[2,1]];
            global_coef_mat[coupling_ind] = dl_tri;
          } else {
            let rl_tri: T = adjacent_stiffnesses[1][[0,1]];
            let dl_tri: T = adjacent_stiffnesses[0][[2,1]];
            global_coef_mat[coupling_ind] = rl_tri + dl_tri;
          }
        // Last row
        } else if space_ind[0] == self.space.row_stride() {
          global_coef_mat[coupling_ind] = typed_zero;
        } else {
          let rl_tri: T = adjacent_stiffnesses[4][[0,1]];
          let dl_tri: T = adjacent_stiffnesses[3][[2,1]];
          global_coef_mat[coupling_ind] = rl_tri + dl_tri;
        }
      }

      // Matrix is symmetric, so insert into transposed coordinate also
      global_coef_mat[[coupling_ind[1], coupling_ind[0]]] =
        global_coef_mat[coupling_ind];      
    }

    global_coef_mat
  }

  fn calc_potentials(&self) -> Vec<T> {
    // let tri_els: Vec<TriEl<T>> = self.generate_elements();
    let coupling_mat: Matrix<T> = self.global_coef_matrix();

    // algorithm:
    //
    // 1. find all unknown potentials indices in space (0 is treated as unknown
    // for now)
    let unknown_potential_inds: Vec<usize> =
      self.potential
          .data()
          .iter()
          .enumerate()
          .filter_map(|(ind, &potential)| {
            if potential == NumCast::from(0).unwrap() {
              Some(ind.clone())
            } else {
              None
            }
          })
          .collect();

    let typed_zero: T = NumCast::from(0.0).unwrap();
    let typed_one: T = NumCast::from(1.0).unwrap();

    let mut final_potentials: Vec<T> = self.potential.clone().into_vec();
    
    while {
      // 6. repeat (1)-(5) on each unknown potential until values converge.
      let previous_unknowns: Vec<T> = unknown_potential_inds.iter().map(|&ind| {
        final_potentials[ind]
      }).collect();

      for &free_ind in &unknown_potential_inds {
        // Previously calculated unknowns to check for convergence
        // 2. get coupling value for that index
        let coupling_val = coupling_mat[[free_ind, free_ind]];
        // 3. get coupling row for that index sans own coupling value
        let mut cur_coupling_row = coupling_mat
          .row(free_ind)
          .raw_slice()
          .to_vec();

        cur_coupling_row.remove(free_ind);
        // 4. get corresponding voltages for same coupling row, sans target
        // voltage
        // TODO reduce copying
        let mut cur_potential_row = final_potentials.clone();

        cur_potential_row.remove(free_ind);

        // 5. compute inner product of (3) and (4), times negative reciprocal of
        // coupling value.
        final_potentials[free_ind] = typed_one / coupling_val *
          inner_product(cur_coupling_row, cur_potential_row);

      }

      // Check for convergence at the end of the while loop. BLACKMAGIC from
      // https://gist.github.com/huonw/8435502.
      previous_unknowns
        .iter()
        .zip(final_potentials.iter())
        .fold(typed_zero, |sum, (&prev_val, &cur_val)| {
          sum + prev_val - cur_val
        }) > NumCast::from(50).unwrap()
    } {}
    final_potentials
  }
}

fn inner_product<V>(first_vec: Vec<V>, second_vec: Vec<V>) -> V
  where V: NumCast + Float {
  
  if first_vec.len() != second_vec.len() {
    panic!("Can't compute the inner product of vectors of unequal length");
  } else {
    let typed_zero: V = NumCast::from(0.0).unwrap();
    first_vec
      .iter()
      .zip(second_vec.iter())
      .fold(typed_zero, |sum, (&val_1, &val_2)| sum + val_1 * val_2)
  }
}

fn lower_diag_inds(nrows: usize) -> Vec<[usize; 2]> {
  (0..nrows)
    .enumerate()
    .flat_map(|(iter_ind, row_ind)| {
      (0..iter_ind + 1).map(move |col_ind| {
        [row_ind, col_ind]
      })
    }).collect()
}

fn upper_diag_inds(nrows: usize, ncols: usize) -> Vec<[usize; 2]> {
  (0..nrows)
    .enumerate()
    .flat_map(|(iter_ind, row_ind)| {
      (iter_ind..ncols).map(move |col_ind| {
        [row_ind, col_ind]
      })
    }).collect()
}

// Computes the triangular number function.
fn triangular(count: usize) -> usize {
  (count * (count + 1)) / 2
}

// Computes the inverse of the triangular number function. Returns Result,
// because not every number is triangular.
fn triangular_inverse(tri_num: usize) -> Result<usize, Error> {
  let test_num = (((tri_num * 8 + 1) as f64).sqrt() - 1.0) / 2.0;
  if test_num == test_num.floor() {
    Ok(test_num as usize)
  } else {
    Err(Error::new(ErrorKind::InvalidArg,
                   format!("Input {} was not triangular.", &tri_num)))
  }
}

// Assumes that the values are passed in row-major order, with all values below
// the diagonal omitted.
fn symmetric_matrix<T>(values: Vec<T>) -> Matrix<T>
  where T: NumCast + Float + Any + Eq {

  // Panics here if the number of values for the upper diagonal is not a
  // triangular number.
  let dim: usize = triangular_inverse(values.len()).unwrap();

  let mut mat: Matrix<T> = Matrix::zeros(dim, dim);

  for (val_ind, &upper_diag_ind) in upper_diag_inds(dim, dim).iter().enumerate() {
    mat[upper_diag_ind] = values[val_ind];

    // Add elements to transpose
    if upper_diag_ind[0] != upper_diag_ind[1] {
      mat[[upper_diag_ind[1], upper_diag_ind[0]]] = values[val_ind];
    }
  }
  mat
}

// Produces a matrix with values only on the main and upper diagonals, with
// zeros on the lower diagonal.
//
// Assumes that the values are passed in row-major order, with all values below
// the diagonal omitted.
fn upper_diag_matrix<T>(values: Vec<T>) -> Matrix<T>
  where T: NumCast + Float + Any + Eq {

  // Panics here if the number of values for the upper diagonal is not a
  // triangular number.
  let dim: usize = triangular_inverse(values.len()).unwrap();

  let mut mat: Matrix<T> = Matrix::zeros(dim, dim);

  for (val_ind, &upper_diag_ind) in upper_diag_inds(dim, dim).iter().enumerate() {
    mat[upper_diag_ind] = values[val_ind];
  }
  mat
}

// Produces a matrix with values only on the main and lower diagonals, with
// zeros on the upper diagonal.
//
// Assumes that the values are passed in row-major order, with all values below
// the diagonal omitted.
fn lower_diag_matrix<T>(values: Vec<T>) -> Matrix<T>
  where T: NumCast + Float + Any + Eq {

  // Panics here if the number of values for the upper diagonal is not a
  // triangular number.
  let dim: usize = triangular_inverse(values.len()).unwrap();

  let mut mat: Matrix<T> = Matrix::zeros(dim, dim);

  for (val_ind, &lower_diag_ind) in lower_diag_inds(dim).iter().enumerate() {
    mat[lower_diag_ind] = values[val_ind];
  }
  mat
}

/// Computes the row-major index of a matrix from its 2D row/col indices.
fn coord_to_ind(pair_coord: &[usize; 2], row_stride: &usize) -> usize {
  pair_coord[0] * row_stride + pair_coord[1]
}

/// Computes the row/col coordinates of a matrix from the node index and
/// knowledge of the row stride.
fn ind_to_coord(node_ind: &usize, row_stride: &usize) -> [usize; 2] {
  [node_ind / row_stride,
   node_ind % row_stride]
}
