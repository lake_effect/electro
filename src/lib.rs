//! Functions for computational electromagnetics.
//!
//! 

#![feature(inclusive_range_syntax)]
#![feature(step_by)]
#![feature(slice_patterns)]

extern crate rulinalg;
extern crate num;

use num::{Float, NumCast};

use rulinalg::matrix::{BaseMatrix, Matrix};

use topo::{pairwise_dist, uniform_grid};

/// Grid struct for all calculations. The generic type `T` refers to the type
/// storing electrical values, and the generic type `U` refers to the type
/// storing spatial values. Generally speaking, `U` should be able to cast 'up'
/// to `T`. Other structs interacting with ElectroGrid use the same convention
/// and signature.
pub struct ElectroGrid<T,U>
  where T: Float, U: Float {
  /// Represents the "true" position of grid elements in space.
  pub space: Matrix<[U; 2]>,
  /// Represents the potential of grid elements.
  pub potential: Matrix<T>,
  /// Represents the electric permittivity of grid elements.
  pub permittivity: Matrix<T>,
  /// Represents the charge on an element.
  pub charge: Option<Matrix<T>>
}


impl<T, U> ElectroGrid<T,U>
  where T: Float, U: Float {
  /// Creates a uniformly spaced grid with the electrical permittivity of free
  /// space.
  pub fn uniform_freespace(x_size: U, y_size: U, spacing: U, potential: T) ->
    ElectroGrid<T, U>
    where T: Float, U: Float {

    let space_grid = uniform_grid(spacing, x_size, y_size);
    let row_count = space_grid.rows();
    let col_count = space_grid.cols();

    
    ElectroGrid {
      space: space_grid,
      potential: Matrix::from_fn(row_count,
                                 col_count,
                                 |_,_| { potential.clone() }),
      permittivity: Matrix::ones(row_count, col_count),
      charge: None
    }
  }

  /// Creates a uniformly spaced grid with the electrical permittivity of free
  /// space, with every point at zero potential.
  pub fn uniform_freespace_zero(x_size: U, y_size: U, spacing: U) ->
    ElectroGrid<T, U>
    where T: Float, U: Float {

    ElectroGrid::uniform_freespace(x_size, y_size, spacing,
                                   NumCast::from(0).unwrap())
  }

  /// Creates a uniformly spaced grid with the electrical permittivity of free
  /// space, with every point at unit potential.
  pub fn uniform_freespace_unit(x_size: U, y_size: U, spacing: U) ->
    ElectroGrid<T, U>
    where T: Float, U: Float {

    ElectroGrid::uniform_freespace(x_size, y_size, spacing,
                                   NumCast::from(1).unwrap())
  }

  /// Computes the area of a spatial element based on its distance from its
  /// contiguous neighbors. Assumes grid elements have borders halfway from each
  /// other.
  ///
  /// TODO memoize?!
  pub fn space_area(&self, element_ind: [usize; 2]) -> U {
    // For each neighbor, check if it exists and then compute the distance.
    let two = NumCast::from(2.0).unwrap();
    
    let mut left_dist = NumCast::from(0.0).unwrap();
    let mut right_dist = NumCast::from(0.0).unwrap();
    let mut up_dist = NumCast::from(0.0).unwrap();
    let mut down_dist = NumCast::from(0.0).unwrap();
    // Left neighbor
    if element_ind[0] > 0 {
      let left_neighbor = self.space[[element_ind[0] - 1, element_ind[1]]];
      left_dist = pairwise_dist(&self.space[element_ind], left_neighbor) / two;
    }
    // Down neighbor
    if element_ind[0] < self.space.rows() - 1 {
      let right_neighbor = self.space[[element_ind[0] + 1, element_ind[1]]];
      right_dist = pairwise_dist(&self.space[element_ind], right_neighbor) / two;
    }
    // Up neighbor
    if element_ind[1] > 0 {
      let up_neighbor = self.space[[element_ind[0], element_ind[1] - 1]];
      up_dist = pairwise_dist(&self.space[element_ind], up_neighbor) / two;
    }
    // Right neighbor
    if element_ind[1] < self.space.cols() - 1 {
      let down_neighbor = self.space[[element_ind[0], element_ind[1] + 1]];
      down_dist = pairwise_dist(&self.space[element_ind], down_neighbor) / two;
    }

    NumCast::from((left_dist + right_dist) * (up_dist + down_dist)).unwrap()
  }
}

pub mod topo;
pub mod material;
pub mod fd;
pub mod mm;
pub mod fem;
