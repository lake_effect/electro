//! Finite difference and other related functions.

#![feature(step_by)]

use std::any::Any;
use std::ops::Mul;

use num::{Float, NumCast};

use rulinalg::matrix::{BaseMatrix, BaseMatrixMut, Matrix, DiagOffset};
use rulinalg::vector::Vector;
use rulinalg::error::Error;

use topo::ccw_rect_from_corners;
use ElectroGrid;

/// Finite difference methods for computing electric potential on a grid.
pub trait FiniteDifference<T, U>
  where T: Mul + NumCast + Float + Any, U: Float {
  /// Computes the potential over a rectangular grid using a finite difference
  /// approximation.
  fn finite_diff_rect(&self) -> Result<ElectroGrid<T,U>, Error>;
  /// Computes a rectangular contour integral over a grid.
  fn contour_int_rect(&self,
                      rect_coords: &[[usize; 2]; 4],
                      pad_inward: bool) -> T;
}

impl<T,U> FiniteDifference<T,U> for ElectroGrid<T,U>
  where T: Mul + NumCast + Float + Any, U: Float {
  /// Computes the potential over a grid represented by a potential field matrix
  /// using a finite difference approximation to Poisson's equation. Takes as
  /// input a matrix representing the physical grid you're modeling, with known
  /// starting values (or zero).
  ///
  /// Returns a `Matrix` of the same dimensions with the potentials computed.
  fn finite_diff_rect(&self) -> Result<ElectroGrid<T,U>, Error> {
    let pot_mat = &self.potential;
    /* algorithm
     *
     * 1. compute node counts
     * 2. create boundary conditions vector
     * 3. create node coefficient matrix
     * 4. solve
     * 5. plots (possibly out of rust)
     * 5a. coefficient node matrix
     * 5b. solved potential matrix
     */
    
    // 1. compute node counts
    let row_count = pot_mat.rows();
    let col_count = pot_mat.cols();
    let node_count = row_count * col_count;
    
    // 3. create boundary conditions vector from potential matrix    
    // This is done by draining the matrix by columns into one long column vector.
    // We have to transpose, as rulinalg stores data in row major form.
    let boundary_vec: Vector<T> = Vector::<T>::from(
      pot_mat.transpose()
             .into_vec()
             .iter()
             .map(|&el| el * NumCast::from(-1.0).unwrap())
             .collect::<Vec<T>>());
    
    // 4. create node coefficient matrix
    let main_diag = Vector::<T>::ones(node_count).apply(&|el| {
      el * NumCast::from(-4.0).unwrap()
    });
    let mut coef_mat = Matrix::from_diag(main_diag.data());
    let typed_zero: T = NumCast::from(1.0).unwrap();
    
    // add neighboring diagonal ones
    for el in coef_mat.diag_iter_mut(DiagOffset::Above(1)) { 
      *el = typed_zero;
    }
    for el in coef_mat.diag_iter_mut(DiagOffset::Below(1)) { 
      *el = typed_zero;
    }
    for el in coef_mat.diag_iter_mut(DiagOffset::Above(col_count)) { 
      *el = typed_zero;
    }
    for el in coef_mat.diag_iter_mut(DiagOffset::Below(col_count)) { 
      *el = typed_zero;
    }

    // remove wrapped nodes. These are places where the coefficient is actually
    // met by a boundary condition (corners and edges)
    let wrapped_coords: Vec<[usize; 2]> = ((col_count)..node_count)
      .step_by(col_count)
      .zip(((col_count - 1)..node_count).step_by(col_count))
      .map(|(val_1, val_2)| [val_1, val_2]) // convert to slice
      .collect();

    for coord in wrapped_coords {
      coef_mat[coord] = NumCast::from(0.0).unwrap();
      let transpose_slice = [coord[1], coord[0]];
      coef_mat[transpose_slice] = NumCast::from(0.0).unwrap();
    }

    // println!("Coefficient matrix: ");
    // println!("");
    // for row in coef_mat.iter_rows() {
    //   println!("{:?}", row);
    // }
    // println!("");
    // 
    // println!("Constant vector: ");
    // for el in boundary_vec.iter() {
    //   println!("{}", el);
    // }

    coef_mat.solve(boundary_vec).and_then(|pot_vec| {
      Ok(ElectroGrid {
        space: self.space.clone(),
        potential: Matrix::new(row_count, col_count, pot_vec).transpose(),
        permittivity: self.permittivity.clone(),
        charge: self.charge.clone()
      })
    })
  }

  /// Computes the integral (sum) over a rectangular contour, starting from the
  /// bottom edge and proceeding counterclockwise.
  fn contour_int_rect(&self,                        
                      rect_coords: &[[usize; 2]; 4],
                      pad_inward: bool) -> T {
    let field_mat = &self.potential;
    let material_mat = &self.permittivity;
    
    let mut padded_coords = rect_coords.clone();
    
    // Check coords to see if they fit inside the matrix
    if rect_coords.iter().any(|coord| {
      coord[0] > field_mat.rows() || coord[1] > field_mat.cols()
    }) {
      if pad_inward {
        for coord in padded_coords.iter_mut() {
          if coord[0] > field_mat.rows() { coord[0] = field_mat.rows(); }
          if coord[1] > field_mat.cols() { coord[1] = field_mat.cols(); }
        }
      } else {
        panic!("Contour integral coordinates didn't fit inside the matrix. \
                They were: {:?}", rect_coords);
      }
    }

    ccw_rect_from_corners(padded_coords)
      .iter()
      .enumerate()
      .fold(NumCast::from(0.0).unwrap(), |acc, (ind, &coord)| {
        // Check to see if the previous or next material is different (different
        // permittivity). If so, divide the permittivity by the number of
        // crossings (1, 2, or 3) and multiply by the coordinate potential
        // amount.
        let mut divider: T = NumCast::from(1.0).unwrap();
        let prev_ind_mod: usize = mod_ind(ind as isize - 1,
                                          padded_coords.len() as isize);
        let next_ind_mod: usize = mod_ind(ind as isize + 1,
                                          padded_coords.len() as isize);

        // Previous material
        // NOTE: This check could be recycled from the previous iteration, but
        // it would make the code harder to read.
        let prev_material = material_mat[padded_coords[prev_ind_mod]];
        if prev_material != material_mat[coord] {
          divider = divider + NumCast::from(1.0).unwrap();
        }

        // Next material
        let next_material = material_mat[padded_coords[next_ind_mod]];
        if next_material != material_mat[coord] {
          divider = divider + NumCast::from(1.0).unwrap();
        }
        
        acc + field_mat[coord] * material_mat[coord] / divider
      })
  }  
}

// Computes the index of an array modulo its length, so you can loop around.
fn mod_ind(val: isize, lim: isize) -> usize {
  ((val + lim) % lim) as usize
}


#[cfg(test)]
mod tests {
  use rulinalg::matrix::Matrix;
  use ElectroGrid;
  use fd::FiniteDifference;
  use topo::uniform_grid;

  #[test]
  fn contour_int_small_square() {
    let test_grid = ElectroGrid::<f64, f64> {
      space: uniform_grid(1.0, 2.0, 2.0),
      potential: Matrix::<f64>::ones(2,2),
      permittivity: Matrix::<f64>::ones(2,2),
      charge: None
    };
    let rect_coords = [[0, 1], [1, 1], [1, 0], [0, 0]];

    assert_eq!(test_grid.contour_int_rect(&rect_coords, false), 4.0);
  }

  #[test]
  fn contour_int_larger_rect() {
    let test_grid = ElectroGrid::<f64, f64> {
      space: uniform_grid(1.0, 4.0, 5.0),
      potential: Matrix::<f64>::zeros(4,5),
      permittivity: Matrix::<f64>::ones(4,5),
      charge: None
    };
    let rect_coords = [[1, 4], [3, 4], [3, 2], [1, 2]];

    assert_eq!(test_grid.contour_int_rect(&rect_coords, false), 0.0);
  }
}
