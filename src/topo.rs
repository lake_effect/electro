//! Functions for producing electrically useful matrix topologies.

extern crate rulinalg;

use rulinalg::matrix::{BaseMatrix, Matrix};
use rulinalg::error::Error;

use num::{Float, NumCast};

use std::f64::consts::PI;
use std::collections::HashSet;
use std::iter::FromIterator;

use ElectroGrid;

/// The circle constant. Also known as `2*pi`.
pub static TAU: f64 = PI + PI;

/// Topology for cylindrical grids.
pub struct Cyl<T, U>
  where T: Float, U: Float {
  /// Structure storing the electrical/physical information (e.g., the cylinder
  /// surface).
  pub grid: ElectroGrid<T, U>,
  /// Radius of the cylinder.
  pub radius: U,
  /// Width of each slice of the cylinder (uniform slicing).
  pub slice_width: U
}

/// Topology for parallel plate (capacitors).
pub struct ParallelPlate<T,U>
  where T: Float, U: Float {
  /// Structure storing the electrical/physical information for the bottom
  /// plate.
  pub bottom_grid: ElectroGrid<T,U>,
  /// Structure storing the electrical/physical information for the top plate.
  pub top_grid: ElectroGrid<T,U>,
  /// Spacing between the two plates.
  pub plate_spacing: U
}

/// Computes the Euclidean distance between two points.
pub fn pairwise_dist<U>(coord_1: &[U; 2], coord_2: [U; 2]) -> U
  where U: Float + NumCast {
  let sub_1: f64 = NumCast::from(coord_1[0] - coord_2[0]).unwrap();
  let sub_2: f64 = NumCast::from(coord_1[1] - coord_2[1]).unwrap();

  let norm: f64 = (sub_1 * sub_1 + sub_2 * sub_2).sqrt();
  
  NumCast::from(norm).unwrap()
}

/// Creates a vector spaced uniformly (as possible within type restrictions).
pub fn lin_vec<U>(min: U, max: U, spacing: U) -> Vec<U>
  where U: NumCast + Float {
  if max < min {
    panic!("Maximum value must be greater than minimum value in lin_vec().");
  }
  
  let approx_count: isize = NumCast::from((max - min) / spacing).unwrap();

  (0..approx_count).map(|step| {
    let step_mult: U = NumCast::from(step).unwrap();
    let true_step: U = min + step_mult * spacing;
    NumCast::from(true_step).unwrap()
  }).collect::<Vec<U>>()
}


/// Creates a grid spaced uniformly (as possible within type restrictions).
pub fn uniform_grid<U>(spacing: U, x_max: U, y_max: U) -> Matrix<[U; 2]>
  where U: NumCast + Float {
  
  let x_size: usize = NumCast::from(x_max / spacing).unwrap();
  let x_coords = lin_vec(NumCast::from(0).unwrap(), x_max, spacing);
  let y_size: usize = NumCast::from(y_max / spacing).unwrap();
  let y_coords = lin_vec(NumCast::from(0).unwrap(), y_max, spacing);
  
  // Since rulinalg::Matrix is row major, we can just enumerate x_coords and
  // fetch the right y_coords element.
  //
  // Below routine from http://stackoverflow.com/a/41308788/2023432
  let coord_vec: Vec<_> = x_coords.iter()
    .flat_map(|&x_coord| {
      y_coords.iter().map(move |&y_coord| {
        [x_coord, y_coord]
      })
    })
    .collect();

  Matrix::new(x_size, y_size, coord_vec)
}

/// Creates a vector of coordinates proceeding in counterclockwise order. The
/// coordinates can be given in any order, but they have to form a rectangle
/// (e.g., share their first and second coordinates with two other coordinate
/// pairs).
pub fn ccw_rect_from_corners(rect_coords: [[usize; 2]; 4]) -> Vec<[usize; 2]> {
  let max_x = rect_coords.iter().max_by_key(|coord| { coord[0] }).unwrap()[0];
  let max_y = rect_coords.iter().max_by_key(|coord| { coord[1] }).unwrap()[1];
  let min_x = rect_coords.iter().min_by_key(|coord| { coord[0] }).unwrap()[0];
  let min_y = rect_coords.iter().min_by_key(|coord| { coord[1] }).unwrap()[1];

  let lower_left = rect_coords
    .iter()
    .find(|&coord| { coord[0] == min_x &&
                     coord[1] == max_y })
    .unwrap();
  let lower_right = rect_coords
    .iter()
    .find(|&coord| { coord[0] == max_x &&
                     coord[1] == max_y })
    .unwrap();
  let upper_right = rect_coords
    .iter()
    .find(|&coord| { coord[0] == max_x &&
                     coord[1] == min_y })
    .unwrap();
  let upper_left = rect_coords
    .iter()
    .find(|&coord| { coord[0] == min_x &&
                     coord[1] == min_y })
    .unwrap();
  
  let bottom_horiz = lower_left[0]...lower_right[0];
  // reverse here because Rust still doesn't do decreasing ranges
  let right_vert = (upper_right[1]...lower_right[1]).rev();
  let top_horiz = (upper_left[0]...upper_right[0]).rev();
  // non-inclusive range because we expect the coordinates to loop back
  let left_vert = upper_left[1]..lower_left[1];

  let mut bottom_vec: Vec<[usize; 2]> = bottom_horiz
    .map(|bottom_x| [bottom_x, rect_coords[0][1]])
    .collect::<Vec<[usize; 2]>>();
  let mut right_vec: Vec<[usize; 2]> = right_vert
    .skip(1)
    .map(|right_y| [rect_coords[1][0], right_y])
    .collect();
  let mut top_vec: Vec<[usize; 2]> = top_horiz
    .skip(1)
    .map(|top_x| [top_x, rect_coords[2][1]])
    .collect();
  let mut left_vec: Vec<[usize; 2]> = left_vert
    .skip(1)
    .map(|left_y| [rect_coords[3][0], left_y])
    .collect();

  bottom_vec.append(&mut right_vec);
  bottom_vec.append(&mut top_vec);
  bottom_vec.append(&mut left_vec);

  bottom_vec
}

/// Fills in a closed shape given by pair coordinates.
///
/// NOTE relies on the shape actually being closed, and possibly, being convex.
fn fill_shape(shape_hull: &mut Vec<[usize; 2]>) {
  let coord_set: HashSet<[usize; 2]> = HashSet::from_iter(
    shape_hull.iter().cloned());
  let row_set: HashSet<usize> = HashSet::from_iter(
    shape_hull.iter().map(|coord| coord[0].clone()));
  let col_set: HashSet<usize> = HashSet::from_iter(
    shape_hull.iter().map(|coord| coord[1].clone()));

  let min_row = row_set.iter().min().unwrap();
  let max_row = row_set.iter().max().unwrap();
  // let min_col = col_set.iter().min().unwrap();
  // let max_col = col_set.iter().max().unwrap();

  for &col_coord in col_set.iter() {
    let mut cur_row = min_row.clone();
    
    while &cur_row < max_row {
      if !coord_set.contains(&[cur_row, col_coord]) {
        shape_hull.push([cur_row, col_coord]);
      }
      cur_row = cur_row + 1;
    }
  }  
}

/// Produces a nearest-neighbor approximation to a circle in 2D grid
/// coordinates.
///
/// TODO replace this with an octagonal approximation described in
/// https://ia800405.us.archive.org/6/items/arxiv-1006.3404/1006.3404.pdf
fn circle_in_grid(radius: &usize,
                  step_count: usize,
                  fill: bool) -> Vec<[usize; 2]> {
  let angle_diff: f64 = TAU / (step_count as f64);
  let mut cur_angle: f64 = 0.0;
  let mut perim_points: Vec<[usize; 2]> = vec![];
  let radius = radius.clone();

  // while loop because step_by(<f64>) doesn't work yet
  while cur_angle < TAU {
    let next_point = [
      (radius as f32 * cur_angle.cos() as f32 + radius as f32) as usize,
      (-1.0 * radius as f32 * cur_angle.sin() as f32 + radius as f32) as usize];

    perim_points.push(next_point);
    cur_angle += angle_diff;
  }

  if fill {
    fill_shape(&mut perim_points)
  }
  
  perim_points
}

/// Produces a centered circular conductor in a new matrix.
pub fn coax(radius: &usize,
            step_count: usize,
            cond_val: f64,
            matrix_dims: &[usize; 2]) -> Result<Matrix<f64>, Error> {
  let mut out_mat = Matrix::<f64>::zeros(matrix_dims[0], matrix_dims[1]);

  let mut coax_points = circle_in_grid(radius, step_count, true);
  for point in &mut coax_points {
    point[0] += (matrix_dims[0] - radius - 1) / 4;
    point[1] += (matrix_dims[1] - radius - 1) / 4;
  }

  for point in coax_points {
    out_mat[point] = cond_val;
  }

  Ok(out_mat)
}

/// Produces a centered circular conductor in a matrix.
pub fn coax_inplace(radius: &usize,
                    step_count: usize,
                    cond_val: f64,
                    matrix: &mut Matrix<f64>) {

  let coax_points = circle_in_grid(radius, step_count, true);

  for point in coax_points {
    matrix[point] = cond_val;
  }
}

/// Produces a microstrip conductor in a new matrix.
pub fn microstrip(strip_width: &usize,
                  strip_val: &f64,
                  matrix_dims: &[usize; 2]) -> Result<Matrix<f64>, Error> {

  let mut out_mat = Matrix::<f64>::zeros(matrix_dims[0], matrix_dims[1]);
  
  let middle_col = out_mat.cols() / 2;
  let conductor_start = middle_col - strip_width / 2;
  let conductor_end = middle_col + strip_width / 2;
  let conductor_col_inds = conductor_start...conductor_end;

  for col_ind in conductor_col_inds {
    out_mat[[0, col_ind]] = strip_val.clone();
  }

  Ok(out_mat)
}

/// Produces a microstrip conductor in a matrix.
pub fn microstrip_inplace(strip_width: &usize,
                          strip_val: &f64,
                          matrix: &mut Matrix<f64>) {

  let middle_col = matrix.cols() / 2;
  let conductor_start = middle_col - strip_width / 2;
  let conductor_end = middle_col + strip_width / 2;
  let conductor_col_inds = conductor_start...conductor_end;

  let last_row_ind = matrix.rows() - 1;

  for col_ind in conductor_col_inds {
    matrix[[0, col_ind]] = strip_val.clone();
  }

  for col_ind in 0..matrix.cols() {
    matrix[[last_row_ind, col_ind]] = 0.0;
  }
}

/// Produces a centered split rectangular conductor in a new matrix.
pub fn split_feed(conductor_width: &usize,
                  conductor_spacing: &usize,
                  conductor_val: &f64,
                  matrix_dims: &[usize; 2]) -> Result<Matrix<f64>, Error> {

  let mut out_mat = Matrix::<f64>::zeros(matrix_dims[0], matrix_dims[1]);
  
  let conductor_rows = [out_mat.rows() / 2 - conductor_spacing,
                        out_mat.rows() / 2 + conductor_spacing];
  let middle_col = out_mat.cols() / 2;
  let conductor_start = middle_col - conductor_width / 2;
  let conductor_end = middle_col + conductor_width / 2;
  let conductor_col_inds = conductor_start...conductor_end;

  for col_ind in conductor_col_inds {
    out_mat[[conductor_rows[0], col_ind]] = conductor_val.clone();
    out_mat[[conductor_rows[1], col_ind]] = conductor_val.clone();
  }

  Ok(out_mat)
}

/// Produces a centered split rectangular conductor in a matrix.
pub fn split_feed_inplace(conductor_width: &usize,
                          conductor_spacing: &usize,
                          conductor_val: &f64,
                          matrix: &mut Matrix<f64>) {

  
  let conductor_rows = [matrix.rows() / 2 - conductor_spacing / 2,
                        matrix.rows() / 2 + conductor_spacing / 2];
  let middle_col = matrix.cols() / 2;
  let conductor_start = middle_col - conductor_width / 2;
  let conductor_end = middle_col + conductor_width / 2;
  let conductor_col_inds = conductor_start...conductor_end;

  for col_ind in conductor_col_inds {
    matrix[[conductor_rows[0], col_ind]] = conductor_val.clone();
    matrix[[conductor_rows[1], col_ind]] = conductor_val.clone();
  }
}

/// Produces a stripline feed in a new matrix.
pub fn stripline(center_cond_width: &usize,
                 center_cond_val: &f64,
                 matrix_dims: &[usize; 2]) -> Result<Matrix<f64>, Error> {

  let mut out_mat = Matrix::<f64>::zeros(matrix_dims[0], matrix_dims[1]);
  
  let middle_row = out_mat.rows() / 2;
  let middle_col = out_mat.cols() / 2;
  let conductor_start = middle_col - center_cond_width / 2;
  let conductor_end = middle_col + center_cond_width / 2;
  let conductor_col_inds = conductor_start...conductor_end;

  for col_ind in conductor_col_inds {
    out_mat[[middle_row, col_ind]] = center_cond_val.clone();
  }

  Ok(out_mat)
}

/// Produces a stripline feed in a matrix.
pub fn stripline_inplace(center_cond_width: &usize,
                         center_cond_val: &f64,
                         matrix: &mut Matrix<f64>) {

  let middle_row = matrix.rows() / 2;  
  let middle_col = matrix.cols() / 2;
  let conductor_start = middle_col - center_cond_width / 2;
  let conductor_end = middle_col + center_cond_width / 2;
  let conductor_col_inds = conductor_start...conductor_end;

  let last_row_ind = matrix.rows() - 1;

  for col_ind in conductor_col_inds {
    matrix[[middle_row, col_ind]] = center_cond_val.clone();
  }

  for col_ind in 0..matrix.cols() {
    matrix[[last_row_ind, col_ind]] = 0.0;
    matrix[[0, col_ind]] = 0.0;
  }
}

/*
 * Private function tests
*/

#[test]
fn rect_small_square() {
  /* 
   * [0, 0]              [1, 0]
   *
   *
   *
   * [0, 1]              [1, 1]
   */
  let coords = [[0, 1], [1, 1], [1, 0], [0, 0]];
  let rect = vec![[0, 1], [1, 1], [1, 0], [0, 0]];
  assert_eq!(rect, ccw_rect_from_corners(coords));
}

#[test]
fn rect_larger() {
  /* 
   * [2, 1]              [8, 1]
   *
   *
   *
   * [2, 5]              [8, 5]
   */

  
  let coords = [[2, 5], [8, 5], [8, 1], [2, 1]];
  let rect = vec![[2, 5], [3, 5], [4, 5], [5, 5], [6, 5], [7, 5], [8, 5],
                  [8, 4], [8, 3], [8, 2], [8, 1],
                  [7, 1], [6, 1], [5, 1], [4, 1], [3, 1], [2, 1],
                  [2, 2], [2, 3], [2, 4]];

  assert_eq!(rect, ccw_rect_from_corners(coords));
}
