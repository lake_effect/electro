# electro

![cargo version badge](https://img.shields.io/crates/v/electro.svg)

a toolkit for basic computational electromagnetics

## what?

This is a library for electrostatics calculation, via:

- [the finite difference method](https://bright-star.gitlab.io/electro/finite-difference/)
- [the method of moments](https://bright-star.gitlab.io/electro/method-of-moments/)
- [the finite element method](https://bright-star.gitlab.io/electro/finite-element/)

## roadmap

### 1.0

- [x] finite difference
- [x] method of moments
- [x] finite element method
- [ ] finite difference (time domain)
- [ ] comprehensive tests

### 2.0

- [ ] Poisson's equation for FEM
- [ ] memoized calls
- [ ] non-rectangular coordinates/topologies (maybe)
