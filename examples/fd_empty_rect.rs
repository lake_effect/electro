extern crate rulinalg;
extern crate electro;

use rulinalg::matrix::BaseMatrix;
use electro::fd::FiniteDifference;
use electro::ElectroGrid;

fn main() {
  // create potential matrix for input to finite diff function

  // grid parameters
  let grid_size = 1;
  let rect_dims: (usize, usize) = (10, 10);
  let row_count = rect_dims.0 / grid_size - 1;
  let col_count = rect_dims.1 / grid_size - 1;

  // rectangular boundary conditions in ESWN order
  let bounds: (f64, f64, f64, f64) = (0.0, 0.0, 0.0, 100.0);

  // create potential matrix (i.e., "physical" mapping)
  let mut grid =
    ElectroGrid::<f64,f64>::uniform_freespace_zero(row_count as f64,
                                                   col_count as f64,
                                                   1.0);
  // Equivalent to
  //
  // let mut grid = ElectroGrid::<f64> {
  //   space: uniform_grid(1.0, row_count as f64, col_count as f64),
  //   spacing: [1,1],
  //   potential: Matrix::<f64>::zeros(row_count, col_count),
  //   permittivity: Matrix::<f64>::ones(row_count, col_count)
  // };
  
  // set boundary values
  for row_ind in 0..row_count {
    grid.potential[[row_ind, col_count - 1]] = bounds.0;
    grid.potential[[row_ind, 0]] = bounds.2;
  }
  for col_ind in 0..col_count {
    grid.potential[[row_count - 1, col_ind]] = bounds.1;
    grid.potential[[0, col_ind]] = bounds.3;
  }
  // 2a. Set corners to averaged corner boundaries if both are nonzero
  if bounds.0 != 0.0 && bounds.1 != 0.0 && bounds.0 != bounds.1 { 
    grid.potential[[row_count - 1, col_count - 1]] = (bounds.0 + bounds.1) / 2.0;
  }
  if bounds.2 != 0.0 && bounds.2 != 0.0 && bounds.2 != bounds.3 { 
    grid.potential[[0, 0]] = (bounds.2 + bounds.3) / 2.0;
  }
  if bounds.0 != 0.0 && bounds.3 != 0.0 && bounds.0 != bounds.3 { 
    grid.potential[[0, col_count - 1]] = (bounds.0 + bounds.3) / 2.0;
  }
  if bounds.1 != 0.0 && bounds.2 != 0.0 && bounds.1 != bounds.2 { 
    grid.potential[[row_count - 1, 0]] = (bounds.1 + bounds.2) / 2.0;
  }

  let computed_grid = grid.finite_diff_rect().unwrap();

  for row in computed_grid.potential.iter_rows() {
    println!("{:?}", row);
    println!("");
  }
}
