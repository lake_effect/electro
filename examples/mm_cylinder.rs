extern crate rulinalg;
extern crate electro;

use rulinalg::matrix::BaseMatrix;
use electro::ElectroGrid;
use electro::topo::Cyl;
use electro::mm::MethodOfMoments;

fn main() {
  let cyl_grid =
    ElectroGrid::<f64,f64>::uniform_freespace_unit(4.0_f64,
                                                   4.0_f64,
                                                   1.0_f64);
  let cyl: Cyl<f64,f64> = Cyl {
    grid: cyl_grid,
    radius: 1.0_f64,
    slice_width: 1.0_f64
  };

  let charge_dist = cyl.moment_comp().unwrap();

  for row in charge_dist.grid.charge.unwrap().iter_rows() {
    println!("{:?}", row);
    println!("");
  }
}
