extern crate rulinalg;
extern crate electro;

use electro::fem::FiniteElementMethod;
use electro::ElectroGrid;

// Example computing the potential of a 2x2 grid square.

fn main() {
  let mut square_grid =
    ElectroGrid::<f64,f64>::uniform_freespace_zero(2.0, 2.0, 1.0);

  // Initial condition: upper left at 100V, lower right at -100V.
  square_grid.potential[[0,0]] = 100.0;
  square_grid.potential[[1,1]] = -100.0;

  // triangular elements
  let elements = square_grid.generate_elements();
  println!("Elements:");
  for tri in elements {
    println!("{:?}", tri);
  }
  println!("");

  let calculated_potentials = square_grid.calc_potentials();

  println!("Calculated potentials: {:?}", &calculated_potentials);
}
