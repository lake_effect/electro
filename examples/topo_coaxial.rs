extern crate electro;
extern crate rulinalg;

use rulinalg::matrix::BaseMatrix;
use electro::topo::coax;

fn main() {
  let coax_mat = coax(&3, 11, 100.0, &[10, 10]).unwrap();
  for row in coax_mat.iter_rows() {
    println!("{:?}", row);
  }
}
