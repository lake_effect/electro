extern crate electro;
extern crate rulinalg;

use rulinalg::matrix::BaseMatrix;
use electro::topo::stripline;

fn main() {
  let stripline_mat = stripline(&4, &100.0, &[10, 10]).unwrap();
  for row in stripline_mat.iter_rows() {
    println!("{:?}", row);
  }
}
