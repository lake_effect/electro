extern crate electro;
extern crate rulinalg;

use rulinalg::matrix::BaseMatrix;
use electro::topo::microstrip;

fn main() {
  let microstrip_mat = microstrip(&4, &100.0, &[10, 10]).unwrap();
  for row in microstrip_mat.iter_rows() {
    println!("{:?}", row);
  }
}
