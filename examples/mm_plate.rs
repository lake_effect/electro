extern crate rulinalg;
extern crate electro;

use rulinalg::matrix::BaseMatrix;
use electro::ElectroGrid;
use electro::topo::ParallelPlate;
use electro::mm::MethodOfMoments;

fn main() {
  let plate_top_grid = ElectroGrid::<f64,f64>::uniform_freespace_unit(4.0_f64,
                                                                  4.0_f64,
                                                                  1.0_f64);

  let plate_bot_grid = ElectroGrid::<f64,f64>::uniform_freespace(4.0_f64,
                                                             4.0_f64,
                                                             1.0_f64,
                                                             -1.0_f64);
  
  let plate: ParallelPlate<f64,f64> = ParallelPlate {
    top_grid: plate_top_grid,
    bottom_grid: plate_bot_grid,
    plate_spacing: 0.5
  };
  
  let charge_dist = plate.moment_comp().unwrap();

  println!("Top plate charge distribution:");
  for row in charge_dist.top_grid.charge.unwrap().iter_rows() {
    println!("{:?}", row);
    println!("");
  }

  println!("Bottom plate charge distribution:");
  for row in charge_dist.bottom_grid.charge.unwrap().iter_rows() {
    println!("{:?}", row);
    println!("");
  }
}
