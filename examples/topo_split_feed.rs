extern crate electro;
extern crate rulinalg;

use rulinalg::matrix::BaseMatrix;
use electro::topo::split_feed;

fn main() {
  let split_feed_mat = split_feed(&4, &2, &100.0, &[10, 10]).unwrap();
  for row in split_feed_mat.iter_rows() {
    println!("{:?}", row);
  }
}
